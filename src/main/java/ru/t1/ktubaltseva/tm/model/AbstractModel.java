package ru.t1.ktubaltseva.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.ktubaltseva.tm.enumerated.Status;

import java.io.Serializable;
import java.util.Date;
import java.util.UUID;

@Setter
@Getter
public class AbstractModel implements Serializable {

    @NotNull
    protected String id = UUID.randomUUID().toString();

    @Nullable
    protected String name;

    @Nullable
    protected String description;

    @NotNull
    protected Status status = Status.NOT_STARTED;

    @NotNull
    protected final Date created = new Date();

    @Nullable
    protected Date dateStart;

    @Nullable
    protected Date dateFinish;

    public AbstractModel(@Nullable final String name) {
        this.name = name;
    }

    public AbstractModel() {
        this.name = id;
    }

    @NotNull
    @Override
    public String toString() {
        @NotNull String result = "";
        result += name;
        if (description != null)
            result += "\t(" + description + ")";
        result += "\t" + Status.toName(status) + "";
        return result;
    }

}

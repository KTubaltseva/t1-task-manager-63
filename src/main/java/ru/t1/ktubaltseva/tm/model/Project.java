package ru.t1.ktubaltseva.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.ktubaltseva.tm.enumerated.Status;

import java.util.Date;

@Getter
@Setter
public final class Project extends AbstractModel {

    private static final long serialVersionUID = 0;

    public Project(@Nullable String name) {
        super(name);
    }

    public Project() {
        super();
    }

}

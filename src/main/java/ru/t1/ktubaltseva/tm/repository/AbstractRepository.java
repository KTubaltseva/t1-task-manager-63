package ru.t1.ktubaltseva.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.ktubaltseva.tm.model.AbstractModel;

import java.util.*;

public abstract class AbstractRepository<M extends AbstractModel> {

    @NotNull
    protected final Map<String, M> models = new LinkedHashMap<>();

    @NotNull
    public M save(@NotNull final M model) {
        models.put(model.getId(), model);
        return model;
    }

    @NotNull
    public M add(@NotNull final M model) {
        models.put(model.getId(), model);
        return model;
    }

    public void clear() {
        models.clear();
    }

    public boolean existsById(@NotNull final String id) {
        return findById(id) != null;
    }

    @NotNull
    public Collection<M> findAll() {
        return models.values();
    }

    public M findById(@NotNull final String id) {
        return models.get(id);
    }

    public void removeById(@NotNull final String id) {
        models.remove(id);
    }

}

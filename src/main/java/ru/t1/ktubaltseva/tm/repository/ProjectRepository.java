package ru.t1.ktubaltseva.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.ktubaltseva.tm.model.Project;
import ru.t1.ktubaltseva.tm.model.Task;

import java.util.Date;

public final class ProjectRepository extends AbstractRepository<Project>  {

    private static final ProjectRepository INSTANCE = new ProjectRepository();

    public static ProjectRepository getInstance() {
        return INSTANCE;
    }

    {
        add(new Project("Project 1"));
        add(new Project("Project 2"));
        add(new Project("Project 3"));
        add(new Project("Project 4"));
    }

    @NotNull
    public Project create() {
        return add(new Project());
    }

}

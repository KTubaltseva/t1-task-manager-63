package ru.t1.ktubaltseva.tm.repository;

import org.jetbrains.annotations.NotNull;
import ru.t1.ktubaltseva.tm.model.Project;
import ru.t1.ktubaltseva.tm.model.Task;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public final class TaskRepository extends AbstractRepository<Task> {

    private static final TaskRepository INSTANCE = new TaskRepository();

    public static TaskRepository getInstance() {
        return INSTANCE;
    }

    {
        add(new Task("Task 1"));
        add(new Task("Task 2"));
        add(new Task("Task 3"));
        add(new Task("Task 4"));
        add(new Task("Task 5"));
        add(new Task("Task 6"));
    }

    @NotNull
    public Task create() {
        return add(new Task());
    }


}

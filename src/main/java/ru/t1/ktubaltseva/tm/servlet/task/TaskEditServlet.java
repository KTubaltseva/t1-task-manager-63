package ru.t1.ktubaltseva.tm.servlet.task;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.ktubaltseva.tm.enumerated.Status;
import ru.t1.ktubaltseva.tm.model.Project;
import ru.t1.ktubaltseva.tm.model.Task;
import ru.t1.ktubaltseva.tm.repository.ProjectRepository;
import ru.t1.ktubaltseva.tm.repository.TaskRepository;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Collection;

@WebServlet({"/task/edit/*"})
public class TaskEditServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        @NotNull final String id = req.getParameter("id");
        @NotNull final Task task = TaskRepository.getInstance().findById(id);
        @NotNull final Collection<Project> projects = ProjectRepository.getInstance().findAll();
        req.setAttribute("task", task);
        req.setAttribute("projects", projects);
        req.setAttribute("statuses", Status.values());
        req.getRequestDispatcher("/WEB-INF/views/task/task-edit.jsp").forward(req,resp);
    }

    @Override
    @SneakyThrows
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        @NotNull final String id = req.getParameter("id");
        @NotNull final String name = req.getParameter("name");
        @NotNull final String description = req.getParameter("description");
        @NotNull final String projectId = req.getParameter("projectId");
        @NotNull final String statusValue = req.getParameter("status");
        @NotNull final Status status = Status.valueOf(statusValue);
        @NotNull final SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        @NotNull final String dateStartValue = req.getParameter("dateStart");
        @NotNull final String dateFinishValue = req.getParameter("dateFinish");

        @NotNull final Task task = new Task();
        task.setId(id);
        task.setName(name);
        task.setDescription(description);
        task.setProjectId(projectId);
        task.setStatus(status);

        if (!dateStartValue.isEmpty()) task.setDateStart(simpleDateFormat.parse(dateStartValue));
        else task.setDateStart(null);

        if (!dateFinishValue.isEmpty()) task.setDateFinish(simpleDateFormat.parse(dateFinishValue));
        else task.setDateFinish(null);

        TaskRepository.getInstance().save(task);
        resp.sendRedirect("/tasks");
    }
}

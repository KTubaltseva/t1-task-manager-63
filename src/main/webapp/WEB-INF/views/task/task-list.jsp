<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<jsp:include page="../../include/_header.jsp"/>

<h1>TASK LIST</h1>

<table width="100%" cellpadding="10" border="1" style="border-collapse: collapse;">
    <tr>
        <th width="200" nowrap="nowrap" align="left">ID</th>
        <th width="200" nowrap="nowrap" align="left">NAME</th>
        <th width="100%" align="left">DESCRIPTION</th>
        <th width="150" nowrap="nowrap" align="left">STATUS</th>
        <th width="200" nowrap="nowrap" align="left">PROJECT</th>
        <th width="100" nowrap="nowrap" align="left">CREATED</th>
        <th width="100" nowrap="nowrap" align="left">START</th>
        <th width="100" nowrap="nowrap" align="left">FINISH</th>
        <th width="100" nowrap="nowrap" align="center">EDIT</th>
        <th swidth="100" nowrap="nowrap" align="center">DELETE</th>
    </tr>
    <c:forEach var="task" items="${tasks}">
        <tr>
            <td>
                <c:out value="${task.id}"/>
            </td>
            <td>
                <c:out value="${task.name}"/>
            </td>
            <td>
                <c:out value="${task.description}"/>
            </td>
            <td>
                <c:out value="${task.status.displayName}"/>
            </td>
            <td>
                <c:out value="${projectRepository.findById(task.projectId).name}"/>
            </td>
            <td>
                <fmt:formatDate pattern="dd.MM.yyyy" value="${task.created}"/>
            </td>
            <td>
                <fmt:formatDate pattern="dd.MM.yyyy" value="${task.dateStart}"/>
            </td>
            <td>
                <fmt:formatDate pattern="dd.MM.yyyy" value="${task.dateFinish}"/>
            </td>
            <td>
                <a href="/task/edit/?id=${task.id}"/>EDIT</a>
            </td>
            <td>
                <a href="/task/delete/?id=${task.id}"/>DELETE</a>
            </td>
        </tr>
    </c:forEach>
</table>

<form action="/task/create" style="padding-top: 20px;">
    <button>CREATE TASK</button>
</form>

<jsp:include page="../../include/_footer.jsp"/>